from iotery_python_server_sdk import Iotery
from flask import Flask, jsonify, request
import os

app = Flask(__name__)

PORT = os.getenv("PORT")

#iotery = Iotery("ZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6STFOaUo5LmV5SnpkV0lpT2lJNE1tWTNNVEF6TkMxbU1qRmxMVEV4WlRrdFlqVTBPQzFrTWpnek5qRXdOall6WldNaUxDSnBZWFFpT2pFMU56RTBOVFE0TkRjc0ltVjRjQ0k2TkRjeU56SXhORGcwTjMwLnpGVnFqRmtnMTJyYW5DQkpSM1BCdVNHUVMxR1BFaENVVzJ5RHFrNzNpSkE=")

#http://localhost:5000/hello
@app.route("/hello/<name>", methods=["GET"])
def get_hello(name):
    return jsonify({"hello":name})

# example from tutorial video -> remove when ready.
@app.route("/number/<integer>", methods=["POST"])
def create_number(integer):
     body = request.json
     return jsonify({"result":integer, "body":body})

@app.route("/devices/<device_uuid>/command-types/<command_type_uuid>", methods=["POST"])
def create_command(device_uuid, command_type):
    res = iotery.createDeviceCommandInstance(deviceUuid=device_uuid, date={"commandTypeUuid": command_type_uuid})
    return res.json()

if __name__ == "__main__":
    app.run(debug=True, port=5000, host="0.0.0.0")