from iotery_embedded_python_sdk import Iotery
from time import time

device_serial = "VAS_DEVICE"
device_key = "VAS_DEVICE"
device_secret = "VAS_DEVICE"
# Get your team ID from https://iotery.io/system
team_id = "82f71034-f21e-11e9-b548-d283610663ec"

device_connector = Iotery()
device_details = device_connector.getDeviceTokenBasic(
    data={"key": device_key, "serial": device_serial, "secret": device_secret, "teamUuid": team_id})
device_connector.set_token(device_details["token"])

command_instances = device_connector.getDeviceUnexecutedCommandInstanceList(
    deviceUuid=device_details["device"]["uuid"])

for command_instance in command_instances["commands"]:
    print(command_instance)
    device_connector.setCommandInstanceAsExecuted(commandInstanceUuid=command_instance["uuid"], data={"timestamp": int(time())})
    print("Command Executed!")