import requests
import json
import time

#----- The below code was used solely for tutorial purposes -------#
#r = requests.post("http://localhost:5000/number/15", 
                #headers={"Content-Type":"application/json"}, 
                #data=json.dumps({"units":"kg", "name":"hello"}))

#print(r.json())
#------------------------------------------------------------------#

# If you are running your VAS on a different port, replace the 5000 with the appropiate port number
base_url = "http://localhost:5000"

vas_response = requests.get(base_url + "/hello/vas")
data = vas_response.json()

# print ("Time is " + str(data["timestamp"]))
# print(data)

device_uuid = "8ae50c3e-f9b1-11e9-b548-d283610663ec"
command_type_uuid = "2a4dce0a-f9c1-11e9-b548-d283610663ec"

# human_time = time.strftime("%a, %d %b %Y %H:%M:%S %Z", 
#                             time.localtime(data["timestamp"]))
# print("Human readable time is " + human_time)

requests.post(base_url + "/devices/" + 
device_uuid + "/command-types/" + command_type_uuid)